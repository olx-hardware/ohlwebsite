# Schedule

| **Week** | **Call type** | **Topic** | **Rough Agenda** |
| --- | --- | --- | --- |
| 0 | Preparation (online 1h) | **Learn the Basics + Self-assessment** | Learn about the building blocks we&#39;ll use during the program<br><br>Complete a self-assessment online |
| 1   | Mentor (30 min)   | **Meet your mentor!** | Get to know each otherGo over participant&#39;s self-assessment<br><br>Project goals&#39; discussion |
| 2 | Mentor (30 min) | **The landscape** | Avoiding a new wheel: review of similar projects<br><br>Open Canvas first draft and review |
| 3 | Mentor (30 min)   | **Open by Design** | Personas &amp; pathways: identifying your users<br><br>Canvas review II: identifying and testing your project hypothesis |
| 4 | Cohort (45 min) | **Peer review** | Present your Canvas to fellow participants &amp; get feedback. |
| 5 | Mentor (30 min)   | **Building for Open** | Planning activities and acquiring resources for your project<br><br>Planning Sprint participation<br><br>Canvas review III |
| 6 | Mentor (30 min)   | **Participation &amp; Inclusion** | Lowering barriers for contribution: Who is and who is not participating in your project?<br><br>Strategies towards building welcoming spaces online and offline |
| 7 | Mentor (30 min)   | **Designing  community** | Bringing value to your community.<br><br>Governance models<br><br>Planning communications and outreach |
| 8 | Cohort (45 min) | **Peer review** | Present your strategy to fellow participants &amp; get feedback. |
| 9 | Mentor (30 min)  | **Roadmapping** | Managing release cycles<br><br>Open hardware licenses |
| 10 | Mentor (30 min)  | **Standards and data** | Make your project findable with OpenKnowHow<br><br>Implementing FAIR data |
| 11 | Mentor (30 min) | **Sprint preparation** | Prepare for the external world!<br><br>Issues, contribution guidelines and community guidelines review |
| 12 | Cohort (45 min)  | **Global Sprint** | Attract contributors from diverse open hardware communities to your project |
| 13 | Mentor (30 min) | **Moving forward** | Feedback from sprint<br><br>Ideas for the future of the project |
| 14 | Public call | **Final Demos** | Celebrate and communicate your project to the world! |
